package com.experis.se.mbfj.assignment2;

import com.experis.se.mbfj.assignment2.models.CountryMostCustomers;
import com.experis.se.mbfj.assignment2.models.Customer;
import com.experis.se.mbfj.assignment2.models.HighestSpender;
import com.experis.se.mbfj.assignment2.models.MostPopularGenre;
import com.experis.se.mbfj.assignment2.repository.customer.CustomerRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class TestCustomerRepository {
    @Test
    void findById_Id10_customer(@Autowired CustomerRepositoryImpl customerRepository) {
        var result = customerRepository.findById(10);
        var expected = new Customer(10, "Eduardo", "Martins",
                "Brazil", "01007-010", "+55 (11) 3033-5446", "eduardo@woodstock.com.br");
        assertEquals(expected, result);
    }

    @Test
    void findByName_idGr_AstridGruber(@Autowired CustomerRepositoryImpl customerRepository) {
        var result = customerRepository.findByName("id Gr");
        var expected = List.of(new Customer(7, "Astrid", "Gruber",
                "Austria", "1010", "+43 01 5134505", "astrid.gruber@apple.at"));
        assertEquals(expected, result);
    }

    @Test
    void countryMostCustomers__USA(@Autowired CustomerRepositoryImpl customerRepository) {
        var result = customerRepository.findCountryWithMostCustomers();
        var expected = List.of(new CountryMostCustomers("USA", 13));
        assertEquals(expected, result);
    }

    @Test
    void findHighestSpender__id6(@Autowired CustomerRepositoryImpl customerRepository) {
        var result = customerRepository.findHighestSpender();
        var expected = List.of(new HighestSpender(6, 49.62));
        assertEquals(expected, result);
    }

    @Test
    void findMostPopularGenre_id12_latinAndRock(@Autowired CustomerRepositoryImpl customerRepository) {
        var result = customerRepository.findMostPopularGenre(12);
        var expected = List.of(new MostPopularGenre(12, "Latin", 16),
                new MostPopularGenre(12, "Rock", 16));
        assertEquals(expected, result);
    }

}

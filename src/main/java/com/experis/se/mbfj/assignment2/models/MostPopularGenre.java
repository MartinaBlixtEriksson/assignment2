package com.experis.se.mbfj.assignment2.models;

public record MostPopularGenre(int customerId, String genreName, int genreCount) {
}

package com.experis.se.mbfj.assignment2.repository.customer;

import com.experis.se.mbfj.assignment2.models.CountryMostCustomers;
import com.experis.se.mbfj.assignment2.models.Customer;
import com.experis.se.mbfj.assignment2.models.HighestSpender;
import com.experis.se.mbfj.assignment2.models.MostPopularGenre;
import com.experis.se.mbfj.assignment2.repository.CrudRepository;

import java.sql.SQLException;
import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {

    /**
     * Find a user in the database by name
     * @param name the name to search for in the database
     * @return A list of customers matching the name. Displays their id, first name, last name, country, postal code,
     * phone number and email; an empty array if no customers are found
     */
    List<Customer> findByName(String name);

    /**
     * Find customers in the database with a limit and offset
     * @param limit
     * @param offset
     * @return A list of customers or an empty array if the offset is higher than the total amount of customers
     */
    List<Customer> findByLimitAndOffset(int limit, int offset);

    /**
     * Find the country with the highest number of customers.
     * In case of a tie it displays all countries with the highest amount of customers.
     * @return A list of countries. This displays the country and the number of customers.
     */
    List<CountryMostCustomers> findCountryWithMostCustomers();

    /**
     * Find customer with the highest total spending.
     * @return A list of customers. Displays the customerId and their total spending
     */
    List<HighestSpender> findHighestSpender();

    /**
     * Find the most popular genre for a specific customer.
     * In case of a tie a list of the top genres is shown.
     * @param customerId id of the customer
     * @return A list of customers displaying customer id, genre name and the number of tracks purchased from the genre.
     */
    List<MostPopularGenre> findMostPopularGenre(int customerId);
}

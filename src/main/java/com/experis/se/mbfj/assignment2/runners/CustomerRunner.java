package com.experis.se.mbfj.assignment2.runners;

import com.experis.se.mbfj.assignment2.models.Customer;
import com.experis.se.mbfj.assignment2.repository.customer.CustomerDataException;
import com.experis.se.mbfj.assignment2.repository.customer.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class CustomerRunner implements ApplicationRunner {
    private final CustomerRepository customerRepository;

    public CustomerRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) {
        try {
            System.out.println(customerRepository.findAll());
//        System.out.println(customerRepository.findById(12));
//        System.out.println(customerRepository.findByName("Martina B"));
//        System.out.println(customerRepository.findByLimitAndOffset(1, 10));
//        customerRepository.insert(new Customer("Martina", "B", "Sweden",
//                "22352533", "05987-12412", "martina@blixt.com"));//
//        customerRepository.update(new Customer(62,"Martina", "Blixt", "Sweden",
//            "23253", "05987-12412", "martina@blixt.com" ));
//        System.out.println(customerRepository.findAll());
//        System.out.println(customerRepository.findCountryWithMostCustomers());
//        System.out.println(customerRepository.findHighestSpender());
//        System.out.println(customerRepository.findMostPopularGenre(5));
        } catch (CustomerDataException e) {
            System.out.println("things went horribly wrong.");
        }
    }
}

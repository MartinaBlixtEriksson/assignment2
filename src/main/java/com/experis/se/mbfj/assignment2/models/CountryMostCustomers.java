package com.experis.se.mbfj.assignment2.models;

public record CountryMostCustomers(String country, int numberOfCustomers) {
}

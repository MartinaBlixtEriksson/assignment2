package com.experis.se.mbfj.assignment2.models;

public record Customer(int customerId, String firstName, String lastName, String country, String postalCode, String phone, String email) {
    public Customer( String firstName, String lastName, String country, String postalCode, String phone, String email) {
        this(0, firstName, lastName, country,postalCode,phone,email);
    }
}

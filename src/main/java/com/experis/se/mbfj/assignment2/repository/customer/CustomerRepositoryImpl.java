package com.experis.se.mbfj.assignment2.repository.customer;

import com.experis.se.mbfj.assignment2.models.CountryMostCustomers;
import com.experis.se.mbfj.assignment2.models.Customer;
import com.experis.se.mbfj.assignment2.models.HighestSpender;
import com.experis.se.mbfj.assignment2.models.MostPopularGenre;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private static final Logger log = LoggerFactory.getLogger(CustomerRepositoryImpl.class);
    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public List<Customer> findAll() {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            ArrayList<Customer> customerList = new ArrayList<>();
            String sql = """
                    SELECT customer_id, first_name, last_name, country, postal_code, phone, email
                    FROM customer
                    """;

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Customer customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customerList.add(customer);
            }

            return customerList;
        } catch (SQLException e) {
            log.error("findAll(), " + e.getMessage());
            throw new CustomerDataException("Database connection failure.");
        }
    }

    @Override
    public Customer findById(Integer id) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            Customer customer = null;
            String sql = """
                    SELECT customer_id, first_name, last_name, country, postal_code, phone, email
                    FROM customer
                    WHERE customer.customer_id = ?
                    """;

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }

            return customer;
        } catch (SQLException e) {
            log.error("findById(Integer id), " + e.getMessage());
            throw new CustomerDataException("Database connection failure.");
        }
    }

    @Override
    public List<Customer> findByName(String name) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            ArrayList<Customer> customerList = new ArrayList<>();
            String sql = """
                    SELECT customer_id, first_name, last_name, country, postal_code, phone, email
                    FROM customer
                    WHERE CONCAT (first_name, ' ', last_name)
                    LIKE ?
                    """;

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, "%" + name + "%");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Customer customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customerList.add(customer);
            }

            return customerList;
        } catch (SQLException e) {
            log.error("findByName(String name), " + e.getMessage());
            throw new CustomerDataException("Database connection failure.");
        }
    }

    @Override
    public List<Customer> findByLimitAndOffset(int limit, int offset) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            ArrayList<Customer> customerList = new ArrayList<>();
            String sql = """
                    SELECT customer_id, first_name, last_name, country, postal_code, phone, email
                    FROM customer
                    LIMIT ?
                    OFFSET ?
                    """;

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Customer customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customerList.add(customer);
            }

            return customerList;
        } catch (SQLException e) {
            log.error("findByLimitAndOffset(int limit, int offset), " + e.getMessage());
            throw new CustomerDataException("Database connection failure.");
        }
    }

    @Override
    public int insert(Customer customer) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            int rowsAffected = 0;
            String sql = """
                    INSERT INTO customer (first_name, last_name, country, postal_code, phone, email)
                    values (?, ?, ?, ?, ?, ?)
                    """;
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customer.firstName());
            preparedStatement.setString(2, customer.lastName());
            preparedStatement.setString(3, customer.country());
            preparedStatement.setString(4, customer.postalCode());
            preparedStatement.setString(5, customer.phone());
            preparedStatement.setString(6, customer.email());
            rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected;
        } catch (SQLException e) {
            log.error("insert(Customer customer), " + e.getMessage());
            throw new CustomerDataException("Database connection failure.");
        }
    }

    @Override
    public int update(Customer customer) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            int rowsAffected = 0;
            String sql = """
                    UPDATE customer
                    SET
                        first_name = ?,
                        last_name = ?,
                        country = ?,
                        postal_code = ?,
                        phone = ?,
                        email = ?
                    WHERE customer_id = ?
                    """;

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customer.firstName());
            preparedStatement.setString(2, customer.lastName());
            preparedStatement.setString(3, customer.country());
            preparedStatement.setString(4, customer.postalCode());
            preparedStatement.setString(5, customer.phone());
            preparedStatement.setString(6, customer.email());
            preparedStatement.setInt(7, customer.customerId());
            rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected;
        } catch (SQLException e) {
            log.error("update(Customer customer), " + e.getMessage());
            throw new CustomerDataException("Database connection failure.");
        }
    }

    @Override
    public List<CountryMostCustomers> findCountryWithMostCustomers() {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            ArrayList<CountryMostCustomers> countryMostCustomers = new ArrayList<>();
            String sql = """
                    SELECT country, COUNT(customer.country) AS number_of_customers
                    FROM customer
                    GROUP BY country
                    ORDER BY number_of_customers DESC
                    FETCH FIRST 1 ROWS WITH TIES
                    """;
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                CountryMostCustomers country = new CountryMostCustomers(
                        resultSet.getString("country"),
                        resultSet.getInt("number_of_customers"));
                countryMostCustomers.add(country);
            }

            return countryMostCustomers;
        } catch (SQLException e) {
            log.error("findCountryWithMostCustomers(), " + e.getMessage());
            throw new CustomerDataException("Database connection failure.");
        }
    }

    @Override
    public List<HighestSpender> findHighestSpender() {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            List<HighestSpender> highestSpenders = new ArrayList<>();
            String sql = """
                    SELECT customer.customer_id, SUM(invoice.total) AS total_spending
                    FROM invoice JOIN customer ON invoice.customer_id = customer.customer_id
                    GROUP BY customer.customer_id
                    ORDER BY total_spending DESC
                    FETCH FIRST 1 ROWS WITH TIES;
                    """;
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                highestSpenders.add(
                        new HighestSpender(
                                resultSet.getInt("customer_id"),
                                resultSet.getDouble("total_spending")
                        )
                );
            }

            return highestSpenders;
        } catch (SQLException e) {
            log.error("findHighestSpender(), " + e.getMessage());
            throw new CustomerDataException("Database connection failure.");
        }
    }

    @Override
    public List<MostPopularGenre> findMostPopularGenre(int customerId) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            List<MostPopularGenre> mostPopularGenres = new ArrayList<>();
            String sql = """
                    SELECT invoice.customer_id, genre.name, COUNT(genre.name) genre_count
                    FROM
                             invoice JOIN invoice_line ON invoice.invoice_id = invoice_line.invoice_id
                             JOIN track ON invoice_line.track_id = track.track_id
                             JOIN genre ON track.genre_id = genre.genre_id
                    WHERE invoice.customer_id = ?
                    GROUP BY invoice.customer_id, genre.name
                    ORDER BY genre_count DESC
                    FETCH FIRST 1 ROWS WITH TIES;
                    """;

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, customerId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                mostPopularGenres.add(
                        new MostPopularGenre(
                                resultSet.getInt("customer_id"),
                                resultSet.getString("name"),
                                resultSet.getInt("genre_count")
                        )
                );
            }

            return mostPopularGenres;
        } catch (SQLException e) {
            log.error("findMostPopularGenre(int customerId), " + e.getMessage());
            throw new CustomerDataException("Database connection failure.");
        }
    }

    @Override
    public int delete(Customer object) {
        log.error("delete(Customer object), Delete is not implemented.");
        throw new UnsupportedOperationException("Delete is not implemented in this repository.");
    }

    @Override
    public int deleteById(Integer integer) {
        log.error("deleteById(Integer integer), Delete is not implemented.");
        throw new UnsupportedOperationException("Delete is not implemented in this repository.");
    }
}

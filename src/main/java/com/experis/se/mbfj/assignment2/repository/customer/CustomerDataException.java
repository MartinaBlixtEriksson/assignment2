package com.experis.se.mbfj.assignment2.repository.customer;

/**
 * The purpose of this exception is to be thrown when an implementation
 * of CustomerRepository encounters an error that needs to be signaled
 * to the caller, e.g., when a database connection fails.
 */
public class CustomerDataException extends RuntimeException {
    public CustomerDataException(String message) {
        super(message);
    }
}

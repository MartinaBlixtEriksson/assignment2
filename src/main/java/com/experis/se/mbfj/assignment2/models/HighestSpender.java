package com.experis.se.mbfj.assignment2.models;

public record HighestSpender(int customerId, double totalSpending) {
}

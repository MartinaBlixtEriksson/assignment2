# Assignment 2 Java Data Access with JDBC

A Spring Boot application where the repository pattern is used to access the "Chinook" Postgres database.
The Chinook database contains customers and music, and purchases of that music by the customers.

The purpose of the repository pattern is to abstract data access away from the underlying
implementation. For example, a method called "findUserById" could get that user from a database
or a local file, and ideally these different implementations should behave in the same way.

This is achieved by defining interfaces with the abstract methods that a class must implement
in order to provide access to the data in question. In this assignment, two interfaces are used:

```java
public interface CrudRepository<T, ID> {
    List<T> findAll();
    T findById(ID id);
    int insert(T object);
    int update(T object);
    int delete(T object);
    int deleteById(ID id);
}
```

```java
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    List<Customer> findByName(String name);

    List<Customer> findByLimitAndOffset(int limit, int offset);

    List<CountryMostCustomers> findCountryWithMostCustomers();

    List<HighestSpender> findHighestSpender();

    List<MostPopularGenre> findMostPopularGenre(int customerId);
}
```

CrudRepository implements the basic CRUD (Create, Read, Update, Delete) operations that all
repositories should have. CustomerReposity extends CrudRepository with methods that are
specific to that repository. One potential issue with this design is the fact that not all
repositories necessarily want to use everything in the CrudRepository interface. In this
assignment for exampel, there is no requirement to implement delete, but due to
CrudRepository having this method, an empty implementation must still be provided.

In our case, we have one implementation of CustomerRepository that happens to provide
the data via a Postgres database.

## Authors

[Martina Blixt Eriksson](@MartinaBlixtEriksson)

[Fredrik Jeppsson](@fredjepp)

## Installation

Insert your local database details in the application.properties file:

```bash
spring.datasource.url=<database url>
spring.datasource.username=<username>
spring.datasource.password=<password>
logging.file.name=<log file if logging to file is desired>
```

To install dependencies and run the tests:

```bash
mvn test
```

Ensure that Maven is installed in available in the path before running mvn test.

Use class CustomerRunner to manually use the app.




INSERT INTO "power" (power_name, power_description) 
VALUES('Innate Capability','Ability To Know Or Understand Something Without The Need Of Studying Or Previous Experience');

INSERT INTO "power" (power_name, power_description) 
VALUES('Superhuman Tracking','Ability To Track An Individual Or Object Through Supernatural Means; Sometimes Referred To As Pathfinding');

INSERT INTO "power" (power_name, power_description) 
VALUES('Superhuman Longevity','Ability To Live Longer Than A Normal Human.');

INSERT INTO "power" (power_name, power_description) 
VALUES('Sonic Scream','Ability To Generate Vocal Sounds Of A Higher Amplitude Than A Normal Human.');

INSERT INTO superhero_power VALUES (1, 2);
INSERT INTO superhero_power VALUES (1, 4);
INSERT INTO superhero_power VALUES (2, 2);
INSERT INTO superhero_power VALUES (3, 1);
INSERT INTO superhero_power VALUES (3, 3);
INSERT INTO superhero (superhero_name, superhero_alias, superhero_origin)
VALUES ('Betty Kane', 'Batgirl', 'Gotham');

INSERT INTO superhero (superhero_name, superhero_alias, superhero_origin)
VALUES ('Kara', 'Supergirl', 'Krypton');

INSERT INTO superhero (superhero_name, superhero_alias, superhero_origin)
VALUES ('Diana', 'Wonder Woman', 'Paradise Island');
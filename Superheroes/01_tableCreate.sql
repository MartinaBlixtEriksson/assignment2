CREATE TABLE superhero(
    superhero_id serial PRIMARY KEY,
    superhero_name text NOT NULL,
    superhero_alias text NOT NULL,
    superhero_origin text NOT NULL
);

CREATE TABLE assistant (
    assistant_id serial PRIMARY KEY,
    assistant_name text NOT NULL
);

CREATE TABLE "power" (
    power_id serial PRIMARY KEY,
    power_name text NOT NULL,
    power_description text NOT NULL
);